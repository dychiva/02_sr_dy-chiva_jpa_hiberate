package com.example.hibernate_jpa.controller;


import com.example.hibernate_jpa.model.Article;
import com.example.hibernate_jpa.model.Category;
import com.example.hibernate_jpa.repository.implementation.ArticleRepositoryImp;
import com.example.hibernate_jpa.repository.implementation.CategoryRepositoryImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/article")
public class ArticleController {

    BaseApiResponse<List<Article>> responseList =  new BaseApiResponse<>();
    BaseApiResponse<Article> response =  new BaseApiResponse<>();


    private CategoryRepositoryImp categoryRepositoryImp;

    @Autowired
    public void setCategoryRepositoryImp(CategoryRepositoryImp categoryRepositoryImp) {
        this.categoryRepositoryImp = categoryRepositoryImp;
    }

    @PersistenceContext
    EntityManager em;

    @Autowired
    private ArticleRepositoryImp articleRepositoryImp;


    /**
     * to get all articles with sub json of category
     * @return
     */
    @GetMapping("")
    public ResponseEntity<BaseApiResponse<List<Article>>> getAllArticles(){
        responseList.setStatus(HttpStatus.FOUND);
        responseList.setMessage("Articles has been found");
        responseList.setData(articleRepositoryImp.getAll());
        responseList.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(responseList);
    }


    /**
     * to get specific of article by id
     * @param id : to get id from URL
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<BaseApiResponse<Article>> getOneById(@PathVariable int id){
        response.setMessage("Article has been found");
        response.setStatus(HttpStatus.FOUND);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setData(articleRepositoryImp.getOneById(id));
        return ResponseEntity.ok(response);
    }


    /**
     * to delete specific record of article by id
     * @param id : to get id from URL
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<BaseApiResponse<Void>> delete(@PathVariable("id") int id){
        BaseApiResponse<Void> response = new BaseApiResponse<>();
        Article article = articleRepositoryImp.getOneById(id);
        if(article != null){
            articleRepositoryImp.delete(id);
            response.setMessage("Article has been deleted");
            response.setStatus(HttpStatus.OK);
        }else {
            response.setMessage("Article can't delete");
            response.setStatus(HttpStatus.BAD_REQUEST);
        }
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }


    /**
     * to save new article
     * @param article insert by pass article object
     * @return
     */
    @PostMapping("")
    public ResponseEntity saveArticle(@RequestBody Article article) {
        BaseApiResponse<Article> baseApiResponse=new BaseApiResponse<>();
        Category category=categoryRepositoryImp.getOne(article.getCategory().getId());
        if (category!=null){
            article.setCategory(category);
            Article articles = articleRepositoryImp.save(article);
            baseApiResponse.setMessage("Article has been inserted");
            baseApiResponse.setData(articles);
            baseApiResponse.setStatus(HttpStatus.OK);
            baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(baseApiResponse);
        }else {
            return ResponseEntity.ok(baseApiResponse);
        }
    }


    /**
     * to update article record by id
     * @param article pass object of article
     * @param id pass id from url to update
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity updateArticle(@RequestBody Article article, @PathVariable int id) {
        Category category=categoryRepositoryImp.getOne(article.getCategory().getId());
        article.setCategory(category);
        article.setId(id);
        BaseApiResponse<Article> response = new BaseApiResponse<>();
        Article data =  articleRepositoryImp.update(article,id);
        response.setMessage("Article has been updated");
        response.setData(data);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }


    /**
     * to get all article that filter by category title
     * @param title pass title string in param of url
     * @return
     */
    @GetMapping("/category")
    public ResponseEntity getArticleByCategoryTitle(@RequestParam String title){
        List<Article> articleDto = articleRepositoryImp.getArticleByCategoryTitle(title);
        BaseApiResponse<List<Article>> baseApiResponse=new BaseApiResponse<>();
        baseApiResponse.setData(articleDto);
        baseApiResponse.setStatus(HttpStatus.FOUND);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        baseApiResponse.setMessage("Article has been found");
        return ResponseEntity.ok(baseApiResponse);
    }

}
