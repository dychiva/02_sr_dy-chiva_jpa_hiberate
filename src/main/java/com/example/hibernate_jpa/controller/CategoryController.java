package com.example.hibernate_jpa.controller;

import com.example.hibernate_jpa.model.Category;
import com.example.hibernate_jpa.repository.implementation.CategoryRepositoryImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api/v1/category")
public class CategoryController {

    private CategoryRepositoryImp categoryRepositoryImp;

    @Autowired
    public void setCategoryRepositoryImp(CategoryRepositoryImp categoryRepositoryImp) {
        this.categoryRepositoryImp = categoryRepositoryImp;
    }


    /**
     * to get all categoris with sub json of category
     * @return
     */
    @GetMapping("")
    public ResponseEntity<BaseApiResponse<List<Category>>> getAll(){
        BaseApiResponse<List<Category>> response = new BaseApiResponse<>();
        response.setStatus(HttpStatus.FOUND);
        response.setMessage("Categories have been found");
        response.setData(categoryRepositoryImp.getAll());
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }


    /**
     * to get specific of category by id
     * @param id : to get id from URL
     * @return
     */
    @GetMapping("/{id}")
    public ResponseEntity<BaseApiResponse<Category>> getOne(@PathVariable("id") int id){
        BaseApiResponse<Category> response = new BaseApiResponse<>();
        response.setMessage("Category has been found");
        response.setStatus(HttpStatus.FOUND);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setData(categoryRepositoryImp.getOne(id));
        return ResponseEntity.ok(response);
    }


    /**
     * to save new category
     * @param category insert by pass category object
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Category> save(@RequestBody Category category){
        return new ResponseEntity<>(categoryRepositoryImp.save(category), HttpStatus.OK);
    }




    /**
     * to delete specific record of category by id
     * @param id : to get id from URL
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteCategory(@PathVariable int id) {
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        categoryRepositoryImp.delete(id);
        baseApiResponse.setMessage("Category has been deleted!");
        baseApiResponse.setStatus(HttpStatus.OK);
        baseApiResponse.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(baseApiResponse);
    }



    /**
     * to update category record by id
     * @param category pass object of category
     * @param id pass id from url to update
     * @return
     */
    @PutMapping("{id}")
    public ResponseEntity updateCategory(@RequestBody Category category, @PathVariable int id) {
        BaseApiResponse<Category> response = new BaseApiResponse<>();
        Category data =  categoryRepositoryImp.update(category,id);
        response.setMessage("Category has been updated");
        response.setData(data);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }


}

