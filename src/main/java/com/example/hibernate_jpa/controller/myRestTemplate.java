package com.example.hibernate_jpa.controller;


import com.example.hibernate_jpa.model.Article;
import com.example.hibernate_jpa.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("restTemplate")
public class myRestTemplate {
    private final String url = "http://localhost:8080/api/v1/";
    private RestTemplate restTemplate;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * TODO category crud with RestTemplate
     * @param
     * @return
     */

    @GetMapping("/category")
    public ResponseEntity<String> getAll() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
       ResponseEntity<String> result =restTemplate.exchange(url + "category", HttpMethod.GET, httpEntity, String.class);
        return result;
    }

    @GetMapping("/category/{id}")
    public ResponseEntity<String> getOne(@PathVariable int id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> result = restTemplate.exchange(url + "category/" + id, HttpMethod.GET, httpEntity, String.class);
        return result;
    }

    @PostMapping("/category")
    public ResponseEntity<String> save(@RequestBody Category category) {
        ResponseEntity responseEntity = restTemplate.postForEntity(url + "category", category, String.class);
        return responseEntity;
    }

    @DeleteMapping("/category/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", String.valueOf(id));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity httpEntity = new HttpEntity(httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url + "category/{id}", HttpMethod.DELETE, httpEntity, String.class, map);
        return responseEntity;
    }

    /**
     * TODO article crud with RestTemplate
     * @param
     * @return
     */


    @GetMapping("/article/{id}")
    public ResponseEntity<String> getOneArticleById(@PathVariable int id){
        Map<String,String> map=new HashMap<>();
        map.put("id",String.valueOf(id));
        return restTemplate.getForEntity(url+"article/{id}",String.class,map);
    }


    @PostMapping("/article")
    public ResponseEntity<String> saveArticle(@RequestBody Article article){
        return restTemplate.postForEntity(url+"article",article,String.class);
    }


    @PutMapping("/article/{id}")
    public ResponseEntity<String> updateArticle(@RequestBody Article article,@PathVariable int id){
        HttpHeaders httpHeaders=new HttpHeaders();
        Map<String,String> param=new HashMap<>();
        param.put("id",String.valueOf(id));
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<Article> entity=new HttpEntity<>(article,httpHeaders);
        return restTemplate.exchange(url+"article/{id}",HttpMethod.PUT,entity,String.class,param);
    }


    @DeleteMapping("/article/{id}")
    public ResponseEntity<String> deleteArticle(@PathVariable int id){
        Map<String, String> param=new HashMap<>();
        param.put("id",String.valueOf(id));
        HttpHeaders httpHeaders=new HttpHeaders();
        httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity entity=new HttpEntity(httpHeaders);
        return restTemplate.exchange(url+"article/{id}",HttpMethod.DELETE,entity,String.class,param);
    }


}

