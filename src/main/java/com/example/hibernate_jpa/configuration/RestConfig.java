package com.example.hibernate_jpa.configuration;


import com.example.hibernate_jpa.controller.myRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {

    @Bean
    public RestTemplate myRest(){
     return new RestTemplate();
    }

}
