package com.example.hibernate_jpa.repository.implementation;

import com.example.hibernate_jpa.model.Article;
import com.example.hibernate_jpa.model.Category;
import com.example.hibernate_jpa.repository.CategoryRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Calendar;
import java.util.List;

@Repository
@Transactional
public class CategoryRepositoryImp implements CategoryRepository {

    @PersistenceContext
    EntityManager em;

    @Override
    public List<Category> getAll() {
        TypedQuery<Category> query = em.createQuery("SELECT c FROM Category AS c", Category.class);
        return query.getResultList();
    }

    @Override
    public Category getOne(int id) {
        try {
            TypedQuery<Category> query = em.createQuery("SELECT c FROM Category AS c WHERE c.id LIKE :id", Category.class).setParameter("id",id);
            return query.getSingleResult();
        }catch (NoResultException exception){
            return null;
        }

    }


    @Override
    public Category delete(int id) {
        CriteriaBuilder criteriaBuilder = this.em.getCriteriaBuilder();
        CriteriaDelete<Category> delete = criteriaBuilder.createCriteriaDelete(Category.class);
        Root<Category> cate = delete.from(Category.class);
        delete.where(criteriaBuilder.equal(cate.get("id"),id));
        this.em.createQuery(delete).executeUpdate();
        return null;
    }

    @Override
    public Category save(Category category) {
        em.persist(category);
        return category;
    }

    @Override
    public Category update(Category category, int id) {
        CriteriaBuilder criteriaBuilder = this.em.getCriteriaBuilder();
        CriteriaUpdate<Category> update = criteriaBuilder.createCriteriaUpdate(Category.class);
        Root<Category> cate = update.from(Category.class);
        update.set("title",category.getTitle());
        update.where(criteriaBuilder.equal(cate.get("id"),id));
        this.em.createQuery(update).executeUpdate();
        return category;

//        TypedQuery<Category> query = em.createQuery("UDATE Category c SET c.title = ? WHERE c.id = ?",Category.class)
//                .setParameter(1, category.getTitle())
//                .setParameter(2,id);
//        return query.executeUpdate();
    }
}
