package com.example.hibernate_jpa.repository.implementation;

import com.example.hibernate_jpa.model.Article;
import com.example.hibernate_jpa.model.Category;
import com.example.hibernate_jpa.repository.ArticleRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.lang.reflect.Type;
import java.util.List;

@Repository
@Transactional
public class ArticleRepositoryImp implements ArticleRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Article> getAll() {
        TypedQuery<Article> query = em.createQuery("SELECT a FROM Article AS a", Article.class);
        return  query.getResultList();
    }

    @Override
    public Article getOneById(int id) {
        TypedQuery<Article> nameQuery = em.createNamedQuery("findOneById", Article.class).setParameter("id",id);
        return nameQuery.getSingleResult();
    }


    @Override
    public List<Article> getArticleByCategoryTitle(String title) {
        Query query = em.createQuery("SELECT a FROM Article a  Join Fetch a.category c Where c.title LIKE lower(:title)", Article.class).setParameter("title","%"+title+"%");
        return query.getResultList();
    }

    @Override
    public Article update(Article article,int id) {
        CriteriaBuilder criteriaBuilder = this.em.getCriteriaBuilder();
        CriteriaUpdate<Article> update = criteriaBuilder.createCriteriaUpdate(Article.class);
        Root<Article> cate = update.from(Article.class);
        update.set("author",article.getAuthor());
        update.set("description",article.getDescription());
        update.set("title", article.getTitle());
        update.set("category",article.getCategory());
        update.where(criteriaBuilder.equal(cate.get("id"),id));
        this.em.createQuery(update).executeUpdate();
        return article;
    }


    @Override
    public void delete(int id) {
        CriteriaBuilder cb = this.em.getCriteriaBuilder();
        CriteriaDelete<Article> delete = cb.createCriteriaDelete(Article.class);
        Root e = delete.from(Article.class);
        delete.where(cb.lessThanOrEqualTo(e.get("id"), id));
        this.em.createQuery(delete).executeUpdate();
    }

    @Override
    public Article save(Article article) {
        em.persist(article);
        return article;
    }
}
