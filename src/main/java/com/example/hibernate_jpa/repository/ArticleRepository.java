package com.example.hibernate_jpa.repository;

import com.example.hibernate_jpa.model.Article;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {
    List<Article> getAll();
    Article getOneById(int id);
    List<Article> getArticleByCategoryTitle(String title);
    Article update(Article article, int id);
    void delete(int id);
    Article save(Article article);
}
