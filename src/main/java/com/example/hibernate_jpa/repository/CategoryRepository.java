package com.example.hibernate_jpa.repository;


import com.example.hibernate_jpa.model.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    List<Category> getAll();
    Category getOne(int id);
    Category update(Category category, int id);
    Category delete(int id);
    Category save(Category category);
}
